package com.bootcamp.passive.controllers;

import com.bootcamp.passive.models.documents.Account;
import com.bootcamp.passive.service.IAccountService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/accounts")
public class AccountController {
    @Autowired
    private IAccountService accountService;

    private static final Logger log = LoggerFactory.getLogger(AccountController.class);

    /**
     * Metodo para listar todos los pasivos de ahorro
     * @return
     */
    @GetMapping
    public Flux<Account> getAll(){
        return accountService.findAll();
    }

    /**
     * Metodo para listar un pasivo de ahorro por id
     * @param id
     * @return
     */
    @GetMapping(path= {"{id}"}, produces = {"application/json"})
    public Mono<Account> getById(@PathVariable("id") String id){
        return accountService.findById(id);
    }

    /**
     * Metodo para crear un pasivo de ahorro
     * @param account
     * @return
     */
    @PostMapping
    public Mono<Account> create(@RequestBody Account account){
        return accountService.save(account);
    }

    /**
     * Metodo para actualizar
     * @param id
     * @param account
     * @return
     */
    @PutMapping(path= {"{id}"}, produces = {"application/json"})
    public Mono<Account> update(@PathVariable("id") String id, @RequestBody Account account){
        return accountService.update(id, account);
    }

    /**
     * Metodo para eliminar
     * @param id
     */
    @DeleteMapping(path= {"{id}"}, produces = {"application/json"})
    public void deleteById(@PathVariable("id") String id){
        accountService.delete(id);
    }
}
