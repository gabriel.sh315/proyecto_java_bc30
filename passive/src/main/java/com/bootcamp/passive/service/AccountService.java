package com.bootcamp.passive.service;

import com.bootcamp.passive.models.documents.Account;
import com.bootcamp.passive.repository.IAccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class AccountService implements IAccountService{

    @Autowired
    private IAccountRepository accountRepository;

    @Override
    public Flux<Account> findAll() {
        return accountRepository.findAll();
    }

    @Override
    public Mono<Account> findById(String id) {
        return accountRepository.findById(id).switchIfEmpty(Mono.empty());
    }

    @Override
    public Mono<Account> save(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public Mono<Account> update(String id, Account account) {
        return accountRepository.findById(id).flatMap(account1 -> {
            account.setId(id);
            return accountRepository.save(account);
        }).switchIfEmpty(Mono.empty());
    }

    @Override
    public void delete(String id) {
        accountRepository.deleteById(id).subscribe();
    }
}
