package com.bootcamp.passive.service;

import com.bootcamp.passive.models.documents.Account;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IAccountService {
    Flux<Account> findAll();
    Mono<Account> findById(String id);
    Mono<Account> save(Account account);
    Mono<Account> update(String id, Account account);
    void delete (String id);
}
