package com.bootcamp.passive.models.documents;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="accounts")
public class Account {
    @Id
    private String id;
    private String accountNumber;
    private double accountBalance;
    private String customer;
    private String type;
}
